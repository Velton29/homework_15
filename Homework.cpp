﻿#include<iostream>

void foo(bool c, int n)
{
    std::cout << "\nNumbers: ";
    for (int i = 0; i <= n; i++)
    {
        if (i % 2 == c)
            std::cout << i << " ";
    }
}

int main()
{
    bool c;
    int n;

    std::cout << "Enter number: ";
    std::cin >> n;

    std::cout << "Enter '1' for uneven numbers, or '0' for even numbers: ";
    std::cin >> c;


    foo(c, n);

    std::cout << std::endl;
    return 0;

}